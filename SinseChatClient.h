/* Copyright 2014 Jose Carrillo */

#ifndef SINSECHATCLIENT
#define SINSECHATCLIENT

#include <thread>
using std::thread;
#include <string>
using std::string;
using std::getline;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include "./SinseChatMsg.h"
#include "./TcpSocketClient.h"
#include "./utils.h"


class SinseChatClient {
    public:
        SinseChatClient(const char* ip, int port): connected(false) {
            cli_socket.connect(ip, port);
            connected = true;
        }
        bool login(const string& _username) {
            username = _username;
            SinseChatMsg msg;
            msg.setUsername(username);
            msg.setCommand("login");
            cli_socket.write(msg.toString());
            return true;
        }
        void start() {
            thread read_thread(&SinseChatClient::read, this);
            thread write_thread(&SinseChatClient::write, this);
            write_thread.join();
            read_thread.join();
        }
        void read() {
            SinseChatMsg msg;
            while (connected) {
                msg = SinseChatMsg(cli_socket);
                if (msg.command == "logout") {
                    connected = false;
                    cli_socket.close();
                    cout << "Connection finished" << endl;
                } else {
                    cout << ">>>>>>> " << msg.username << ": ";
                    cout << msg.msg << endl;
                }
            }
        }
        void write() {
            string user_input;     // msg that the user writes
            SinseChatMsg msg;
            while (connected) {
                getline(cin, user_input);
                msg = SinseChatMsg(user_input);

                if (msg.command == "logout") {
                    cli_socket.write(msg.toString());
                    connected = false;
                    cli_socket.close();
                    cout << "Connection finished" << endl;

                } else if (msg.command == "select") {
                    if (msg.msg == ""){
                        cout << "Selected user " << "'" << dest << "'" << endl;
                    } else {
                        dest = msg.msg;
                        cout << "Selected user " << "'" << dest << "'" << endl;
                    }

                } else if (msg.command == "users") {
                    cli_socket.write(msg.toString());

                } else if (msg.command == "send") {
                    if (msg.username == "") {
                        msg.setUsername(dest);
                    }
                    cli_socket.write(msg.toString());
                }
            }
        }

    private:
        string username;
        string dest;
        TcpSocketClient cli_socket;
        bool connected;
};

#endif
