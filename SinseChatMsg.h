/* Copyright 2014 Jose Carrillo */

#ifndef SINSECHATMSGBUILDER_H
#define SINSECHATMSGBUILDER_H

#include <string>
using std::string;
using std::getline;
#include <sstream>
using std::stringstream;

#include "./TcpSocketClient.h"
#include "./TcpSocketServer.h"
#include "./utils.h"


class SinseChatMsg {
    public:
        string username;    // 16 bytes
        string command;     // 8 bytes
        string msg_length;  // 8 bytes
        string msg;         // variable length

        SinseChatMsg(): msg_length("0") {}
        explicit SinseChatMsg(TcpSocketServerConnection con) {
            username = con.read(16);
            command = con.read(8);
            msg_length = con.read(8);
            int n = strtoi(msg_length);
            if (n > 0) {
                msg = con.read(n);
            }
        }
        explicit SinseChatMsg(TcpSocketClient con) {
            username = con.read(16);
            command = con.read(8);
            msg_length = con.read(8);
            int n = strtoi(msg_length);
            msg = con.read(n);
        }
        explicit SinseChatMsg(const string& user_input): msg_length("0") {
            stringstream ss(user_input);
            if (user_input[0] == '/') {
                ss.ignore();    // ignore '/'
                ss >> command;
                ss.ignore();    // ignore space
                getline(ss, msg);
                setMsg(msg);
            } else if (user_input[0] == ':') {
                ss.ignore();    // ignore ':'
                ss >> username;
                ss.ignore();    // ignore space
                getline(ss, msg);
                setMsg(msg);
                setCommand("send");
            } else {
                setCommand("send");
                setMsg(user_input);
            }
        }
        void setUsername(const string& _username) {
            username = _username;
        }
        void setCommand(const string& _command) {
            command = _command;
        }
        void setMsg(const string& _msg) {
            msg = _msg;
            msg_length = itostr(msg.size());
        }

        string toString() {
            string ret;
            ret += fill_right(username, '\0', 16);
            ret += fill_right(command, '\0', 8);
            ret += fill_right(msg_length, '\0', 8);
            ret += msg;
            return ret;
        }
};

#endif
