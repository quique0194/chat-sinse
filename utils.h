/* Copyright 2014 Jose Carrillo */

#ifndef UTILS_H
#define UTILS_H

#include <string>
using std::string;
#include <sstream>
using std::stringstream;
#include <iomanip>
using std::setw;
using std::setfill;

int strtoi(const string& str) {
    return atoi(str.c_str());
}

string itostr(int i, int width = 0) {
    stringstream ss;
    ss << setw(width) << setfill('0') << i;
    return ss.str();
}

string fill_right(string target, char fill, int size) {
    while (target.size() < size) {
        target += fill;
    }
    return target;
}

#endif
