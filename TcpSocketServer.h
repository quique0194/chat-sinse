/* Copyright 2014 Jose Carrillo */

#ifndef TCPSOCKETSERVER_H
#define TCPSOCKETSERVER_H

#include <sys/types.h>      // socket
#include <sys/socket.h>     // socket
#include <arpa/inet.h>      // htons, inet_pton

#include <string.h>         // memset
#include <unistd.h>         // read, write

#include <string>
using std::string;

#include "./exceptions.h"


class TcpSocketServerConnection {
    public:
        explicit TcpSocketServerConnection(int _connection_fd = 0):
                connection_fd(_connection_fd) {
        }
        string read(int size = 1000) {
            char buf[size];
            ::read(connection_fd, buf, size);
            return string(buf);
        }
        void write(const string& msg) {
            ::write(connection_fd, msg.c_str(), msg.size());
        }
        void close() {
            shutdown(connection_fd, SHUT_RDWR);
            ::close(connection_fd);
            connection_fd = 0;
        }
        bool is_connected() {
            return connection_fd != 0;
        }

    private:
        int connection_fd;
};

class TcpSocketServer {
    public:
        TcpSocketServer() {
            socket_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

            if (socket_fd == -1)
                throw CreateSocketException();
        }

        void listen(int port) {
            memset(&stSockAddr, 0, sizeof(struct sockaddr_in));

            stSockAddr.sin_family = AF_INET;
            stSockAddr.sin_port = htons(port);
            stSockAddr.sin_addr.s_addr = INADDR_ANY;

            if (-1 == bind(socket_fd, (const struct sockaddr *)&stSockAddr,
                          sizeof(struct sockaddr_in))) {
                close();
                throw BindingException();
            }
            if (-1 == ::listen(socket_fd, 10)) {
                close();
                throw ListenException();
            }
        }

        void close() {
            shutdown(socket_fd, SHUT_RDWR);
            ::close(socket_fd);
        }

        TcpSocketServerConnection accept() {
            int connection_fd = ::accept(socket_fd, NULL, NULL);
            if (0 > connection_fd) {
                close();
                throw AcceptConnectionException();
            }
            return TcpSocketServerConnection(connection_fd);
        }

    private:
        struct sockaddr_in stSockAddr;
        int socket_fd;
};

#endif
