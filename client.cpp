/* Copyright 2014 Jose Carrillo */

#include <string>
using std::string;

#include "./SinseChatClient.h"


int main(void) {
    SinseChatClient chat("127.0.0.1", 1100);
    string username;
    cout << "Enter username: ";
    cin >> username;
    chat.login(username);
    cout << "Welcome! You're logged in as " << username << endl;
    cin.ignore();
    chat.start();
    return 0;
}
