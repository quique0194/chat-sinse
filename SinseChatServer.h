/* Copyright 2014 Jose Carrillo */

#ifndef SINSECHATSERVER
#define SINSECHATSERVER

#include <thread>
using std::thread;
#include <string>
using std::string;
using std::getline;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <list>
using std::list;
#include <exception>
using std::exception;
#include <map>
using std::map;

#include "./TcpSocketServer.h"
#include "./SinseChatMsg.h"

#define MAX_CONNECTIONS 3

class SinseChatServer {
    public:
        explicit SinseChatServer(int _port): port(_port), cli_inited(false) {
            for (int i = 0; i < MAX_CONNECTIONS; ++i) {
                free_connections.push_back(i);
            }
        }
        void start() {
            thread accept_connections_thread(
                &SinseChatServer::accept_connections, this);
            accept_connections_thread.detach();
            cout << "Chat server started!" << endl;
        }
        // Command Line Inteface
        void start_cli() {
            cli_inited = true;
            while (cli_inited) {
                string command;
                getline(cin, command);
                proc_command(command);
            }
        }
        void proc_command(const string& command) {
            if (command == "stop") {
                stop_cmd();
                return;
            }
        }
        void stop_cmd() {
            stop();
            cout << "Chat server stopped" << endl;
            cli_inited = false;
        }
        // server actions
        void accept_connections() {
            ser_socket.listen(port);
            while (true) {
                if (!free_connections.empty()) {
                    int idx = free_connections.front();
                    try {
                        connections[idx] = ser_socket.accept();
                    } catch(const exception& e) {
                        return;
                    }
                    free_connections.pop_front();
                    thread handle_connection_thread(
                        &SinseChatServer::handle_connection, this, idx);
                    handle_connection_thread.detach();
                }
            }
        }
        void stop() {
            for (int i = 0; i < MAX_CONNECTIONS; ++i) {
                if (connections[i].is_connected()) {
                    SinseChatMsg msg;
                    msg.setCommand("logout");
                    connections[i].write(msg.toString());
                    connections[i].close();
                }
            }
            ser_socket.close();
        }
        void handle_connection(int idx) {
            cout << "Client connected on " << idx << endl;
            SinseChatMsg msg;
            string username;
            while (true) {
                msg = SinseChatMsg(connections[idx]);
                cout << "MSG: " << msg.msg << endl;
                if (msg.command == "logout") {
                    user_cons.erase(username);
                    break;
                } else if (msg.command == "login") {
                    username = msg.username;
                    user_cons[username] = idx;
                } else if (msg.command == "users") {
                    SinseChatMsg reply;
                    reply.setUsername("server");
                    reply.setMsg(get_list_of_users());
                    connections[idx].write(reply.toString());
                } else if (msg.command == "send") {
                    SinseChatMsg reply;
                    if (user_cons.count(msg.username) == 0) {
                        reply.setUsername("server");
                        reply.setMsg("Select a valid destination");
                        reply.setCommand("error");
                        connections[idx].write(reply.toString());
                    } else {
                        int dest = user_cons[msg.username];
                        reply.setUsername(username);
                        reply.setMsg(msg.msg);
                        reply.setCommand("send");
                        connections[dest].write(reply.toString());
                    }
                } else {
                    SinseChatMsg msg;
                    msg.setUsername("server");
                    msg.setMsg("Invalid command");
                    connections[idx].write(msg.toString());
                }
            }
            connections[idx].close();
            free_connections.push_back(idx);
            cout << "Client disconnected on " << idx << endl;
        }
        string get_list_of_users() {
            string ret;
            map<string, int>::iterator it;
            for (it = user_cons.begin(); it != user_cons.end(); ++it) {
                ret += it->first;
                ret += ", ";
            }
            return ret;
        }

    private:
        int port;
        bool cli_inited;    // check if command line interface is running
        TcpSocketServerConnection connections[MAX_CONNECTIONS];
        list<int> free_connections;     // save free connections indexes
        map<string, int> user_cons;     // user connections
        TcpSocketServer ser_socket;
};

#endif
