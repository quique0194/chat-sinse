/* Copyright 2014 Jose Carrillo */

#ifndef TCPSOCKETCLIENT_H
#define TCPSOCKETCLIENT_H

#include <sys/types.h>      // socket
#include <sys/socket.h>     // socket
#include <arpa/inet.h>      // htons, inet_pton

#include <string.h>         // memset
#include <unistd.h>         // read, write

#include <string>
using std::string;

#include "./exceptions.h"


class TcpSocketClient {
    public:
        TcpSocketClient() {
            socket_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

            if (socket_fd == -1)
                throw CreateSocketException();
        }

        void connect(const char* ip, int port) {
            memset(&stSockAddr, 0, sizeof(struct sockaddr_in));
            stSockAddr.sin_family = AF_INET;
            stSockAddr.sin_port = htons(port);
            if (0 == inet_pton(AF_INET, ip, &stSockAddr.sin_addr))
                throw InvalidIpException();

            if (-1 == ::connect(socket_fd,
                                (const struct sockaddr *)&stSockAddr,
                                sizeof(struct sockaddr_in)))
                throw ConnectionException();
        }

        void close() {
            shutdown(socket_fd, SHUT_RDWR);
            ::close(socket_fd);
        }

        void write(const string& msg) {
            ::write(socket_fd, msg.c_str(), msg.size());
        }

        string read(int size = 1000) {
            char buf[size+1];
            ::read(socket_fd, buf, size);
            buf[size] = 0;  // null terminating char
            return string(buf);
        }

    private:
        struct sockaddr_in stSockAddr;
        int socket_fd;
};

#endif
